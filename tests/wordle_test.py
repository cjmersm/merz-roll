import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

from commands.wordle import getWordleScoreFromTxt
from commands.wordle import getWordleScoreForTheWeek
from commands.wordle import userPlayedToday
from helpers.helpers import isWordleInput
from commands.wordle import getAlltimeWinners


def test_empty_file():
    score = getWordleScoreFromTxt("tests/test_data/empty_file.txt", 12345, "user")
    assert score == None


def test_get_score():
    score = getWordleScoreFromTxt("tests/test_data/some_data.txt", 261, "user1")
    assert score == 5
    score = getWordleScoreFromTxt("tests/test_data/some_data.txt", 69, "user1")
    assert score == None
    score = getWordleScoreFromTxt("tests/test_data/some_data.txt", 261, "user69")
    assert score == None


def test_get_wordle_of_week():
    day_of_year = 68
    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/some_data.txt")
    assert len(score_map) == 3
    assert score_map["user3"] == 8
    assert score_map["user2"] == 10
    assert score_map["user1"] == 11

    day_of_year = 66
    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/some_data.txt")
    assert len(score_map) == 2
    assert "user3" not in score_map.keys()
    assert score_map["user1"] == 5
    assert score_map["user2"] == 3

    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/empty_file.txt")
    assert len(score_map) == 0


def test_sort():
    day_of_year = 68
    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/some_data.txt") # should be sorted already
    assert len(score_map) == 3
    ordered_keys = list(score_map.keys())
    assert ordered_keys[0] == 'user3'  # because he hasn't played today
    assert ordered_keys[1] == 'user2'
    assert ordered_keys[2] == 'user1'

    day_of_year = 69
    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/some_data.txt") # should be sorted already
    assert len(score_map) == 3
    ordered_keys = list(score_map.keys())
    assert ordered_keys[0] == 'user2'
    assert ordered_keys[1] == 'user1'
    assert ordered_keys[2] == 'user3'  # because no one has played today

def test_seven():
    day_of_year = 72
    score_map = getWordleScoreForTheWeek(day_of_year, "tests/test_data/seven_days_worth.txt") # should be sorted already
    assert score_map['user3'] == 25
    assert score_map['user2'] == 21
    assert score_map['user1'] == 24
    
    
def test_user_played_today():
    day_of_year = 66
    user_played = userPlayedToday('user3', day_of_year, "tests/test_data/some_data.txt")
    assert not user_played

    user_played = userPlayedToday('user1', day_of_year, "tests/test_data/some_data.txt")
    assert user_played

    user_played = userPlayedToday('user1', day_of_year, "tests/test_data/empty_file.txt")
    assert not user_played

    day_of_year = 67
    user_played = userPlayedToday('user3', day_of_year, "tests/test_data/some_data.txt")
    assert user_played


def test_world_input():
    testCases = [
        (False, "Wordle butts 5/6"),
        (False, "Worlde 285 5/6"),
        (False, "Wordle 286 Y/6"),
        (False, "Wordle 286 5"),
        (False, "Wordle 286 butts"),
        (True, "Wordle 286 X/6"),
        (True, "Wordle 286 6/6"),
        (False, "Wordle 286 0/6"),
        (True, "Wordle 286 1/6"),
        (False, "butts butts butts/6"),
    ]
    for case in testCases:
        assert case[0] == isWordleInput(case[1])


# TODO WRITE TESTS
# def test_winner_map():
#     getAlltimeWinners("../wordle.txt")


print("Running tests")
test_empty_file()
test_get_score()
test_get_wordle_of_week()
test_sort()
test_seven()
test_user_played_today()
test_world_input()
# test_winner_map()
print("Tests successful!")
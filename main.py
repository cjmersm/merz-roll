import discord
import traceback
import aiohttp
import json
import random
import os
# import threading

from commands.roll import buildRollDescriptionAndColor
from commands.magic import magicEightBallResponse
from commands.gif import getRandomDict
from commands.wordle import buildSaveAndBuildWordleResponse, buildWeeklyResults, buildWinnersResponse
from commands.mlm import isMlmEligible, mlmHandleMessage
# from commands.stonks import getQuote

from helpers.helpers import getErrorResponse
from helpers.helpers import getHelpMessage
from helpers.helpers import isWordleInput

from commands.businessfish import getBusinessFishFile

client = discord.Client()


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    content = message.content.lower()

    try:
        ##### !test command #####
        if content.startswith('!test') or content.startswith("!help"):
            await message.channel.send("", embed=discord.Embed(description=getHelpMessage(), color=0xFFFFFF))

        ##### !ping command #####
        if content.startswith('!ping'):
            await message.channel.send("", embed=discord.Embed(description="pong", color=0xFFFFFF))

        ##### !magic command #####
        if content.startswith('!magic'):
            desc, color = magicEightBallResponse()
            await message.channel.send("", embed=discord.Embed(description=desc, color=color))

        ##### !stock command #####
        # DEPRECATED
        # if content.startswith('!stock') or content.startswith('!stonk'):
        #     desc, color = getQuote(content)
        #     await message.channel.send("", embed=discord.Embed(description=desc, color=color))

        ##### MLM ######
        MLM_FILE = "mlm.csv"
        if isMlmEligible(message):
            mlmHandleMessage(MLM_FILE, message)

        ##### Wordle command #####
        WORDLE_FILE = "wordle.txt"
        if isWordleInput(message.content):
            resp = buildSaveAndBuildWordleResponse(message, WORDLE_FILE)
            await message.channel.send("", embed=discord.Embed(description=resp))

        if message.content.lower().startswith('!wordleaderboard') or message.content.lower().startswith('!wlb'):
            resp = buildWeeklyResults(WORDLE_FILE)
            await message.channel.send("", embed=discord.Embed(description=resp))

        if message.content.lower().startswith('!wordlewinnerboard') or message.content.lower().startswith('!wwb'):
            resp = buildWinnersResponse(WORDLE_FILE)
            await message.channel.send("", embed=discord.Embed(description=resp))

        ##### !roll commands #####
        if content.startswith('!roll') or content.startswith('!r'):
            desc, color = buildRollDescriptionAndColor(message)
            await message.channel.send("", embed=discord.Embed(description=desc, color=color))

        ##### !business fish #####
        if content.startswith('!bf') or content.startswith('!businessfish') or content.startswith('!business-fish'):
            if len(content.split()) > 1:
                if content.split()[1] == "reactions":
                    files = os.listdir("media/business-fish")
                    options = "Business fish options include:\n"
                    for file in files:
                        options += f'\t`{file.split(".")[0]}`\n'
                    await message.channel.send(options)
                else:
                    file = getBusinessFishFile(content.split()[1])
                    await message.channel.send(file=discord.File(file))
            else:
                file = getBusinessFishFile("wave")
                await message.channel.send(file=discord.File(file))

        ##### !gif commands #####
        if content.startswith('!gif') or content.startswith('!giphy'):
            embed = discord.Embed(colour=discord.Colour.blue())
            session = aiohttp.ClientSession()

            search = getRandomDict()

            search.replace(' ', '+')
            response = await session.get('http://api.giphy.com/v1/gifs/search?q=' + search + '&api_key=88QJXt7gytQ1V6icN4utPELJ07b4fw6X&limit=10')
            data = json.loads(await response.text())
            gif_choices_length = len(data['data'])
            gif_choice = random.randint(0, gif_choices_length - 1 if gif_choices_length > 0 else 0)
            print(f'Search term {search} at index {gif_choice} with data length {gif_choices_length}')
            print(f'Data: {data["data"][gif_choice]}')
            embed.set_image(url=data['data'][gif_choice]['images']['original']['url'])

            await session.close()
            await message.channel.send("", embed=embed)

    except Exception:
        traceback.print_exc()
        await message.channel.send(getErrorResponse())

client.run(os.getenv('DISCORD_KEY'))

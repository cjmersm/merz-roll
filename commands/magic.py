from random import randrange

randomEightBallColors = [
    0x1abc9c,
    0x11806a,
    0x2ecc71,
    0x1f8b4c,
    0x3498db,
    0x206694,
    0x9b59b6,
    0x71368a,
    0xe91e63,
    0xad1457,
    0xf1c40f,
    0xc27c0e,
    0xe67e22,
    0xa84300,
    0xe74c3c,
    0x992d22,
    0x95a5a6,
    0x607d8b,
    0x979c9f,
    0x546e7a,
    0x7289da,
    0x99aab5]

eightBallResponses = [
    "That’s worse than shredding your balls on a cheese grater.",
    "Don’t count on it.",
    "It is certain.",
    "It is decidedly so.",
    "The answer is 7",
    "Most likely.",
    "My reply is no.",
    "My sources say no.",
    "Outlook not so good.",
    "Outlook good.",
    "Reply hazy, try again.",
    "Signs point to yes.",
    "I should say that you think that you said that I say yes. Or no. Your choice?",
    "Very doubtful.",
    "Without a doubt.",
    "Yes.",
    "Yes – definitely.",
    "You may rely on it."]

def magicEightBallResponse():
    eightBallResponse = eightBallResponses[randrange(len(eightBallResponses))]
    color = randomEightBallColors[randrange(len(randomEightBallColors))]
    return eightBallResponse, color


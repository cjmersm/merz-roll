import os
from datetime import datetime
from dateutil.tz import gettz


def buildSaveAndBuildWordleResponse(message, filename):
    author = str(message.author)
    words = message.content.split()

    wordleId = words[1]
    numberOfAttempts = words[2][0]

    if numberOfAttempts == "X":
        numberOfAttempts = "7"

    try:
        saveWordleScore(wordleId, author, numberOfAttempts, filename)
    except Exception as ex:
        if str(ex).startswith("You have"):
            return str(ex)
        else:
            raise ex

    if numberOfAttempts == "1":
        return f'Good fucking work {author}! Go buy yourself a lottery ticket!'
    if numberOfAttempts == "2":
        return f'Great job {author}! Your score was recorded!'
    if numberOfAttempts == "6":
        return f'That was a close one {author}. Your score was recorded!'
    if numberOfAttempts == "7":
        return f'Ouch {author}. Your score was recorded.'
    return f'Thanks {author}, your score was recorded!'


def buildWinnersResponse(filename):
    winners = getAlltimeWinners(filename) # date, score, winners
    resp = "***Wordle Winners Leaderboard:***\n------------------------------------------\n"

    for winner in winners:
        resp += f'{winner[0]} - **{winner[1]}** - {formatWinners(winner[2])}\n'
    return resp 


def formatWinners(winners):
    output = ""
    for winner in winners:
       output = output + f' {winner.split("#")[0]}'
    return output


def buildWeeklyResults(filename):
    day_of_year = getDayOfYear()
    scoreMap = getWordleScoreForTheWeek(day_of_year, filename)
    resp = "***This Week's Wordle Leaderboard:***\n------------------------------------------\n"

    for user in sortMap(scoreMap):
        emote = ":ballot_box_with_check:"
        if not userPlayedToday(user, day_of_year, filename):
            emote = ":watch:"

        resp += f'**{withPadding(scoreMap[user])}** | {emote} | **{user.split("#")[0]}**\n'
    return resp


def withPadding(score):
    if len(str(score)) == 1:
        return f'` {score} pts`'
    return f'`{score} pts`'


def saveWordleScore(wordleId, author, attempts, filename):
    existingScore = getWordleScoreFromTxt(filename, wordleId, author)
    if existingScore:
        raise Exception(f'You have already submitted a wordle for {wordleId}')
    else:
        answ = os.path.exists(filename)
        wordle_file = open(filename, "a" if answ else "w")
        wordle_file.write(f'{wordleId} {author} {attempts}\n')
        wordle_file.close()
        return


def userPlayedToday(user, day_of_year, filename):
    score_arr = buildScoreArray(filename)
    todays_wordle_id = getTodaysWordleId(day_of_year)

    for score_record in score_arr:
        wordleId = score_record[0]
        author = score_record[1]
        if todays_wordle_id == wordleId and author == user:
            return True

    return False


# day_of_year = datetime.now().timetuple().tm_yday
# well this got complicated quick
# Gets scores of all players that played in the last week since Monday
def getWordleScoreForTheWeek(day_of_year, filename):
    days_back = getDaysBack(day_of_year)
    todaysWordleId = getTodaysWordleId(day_of_year)
    score_arr = buildScoreArray(filename)
    weeklyScoreMap = {}
    gamesPlayedMap = {}

    for iterated_id in range(todaysWordleId - days_back + 1, todaysWordleId + 1):
        for score_record in score_arr:
            wordleId = score_record[0]
            author = score_record[1]
            score = score_record[2]

            if wordleId == iterated_id and author not in weeklyScoreMap.keys():
                weeklyScoreMap[author] = score
                if author not in gamesPlayedMap.keys():  # ugly, need to clean this up
                    gamesPlayedMap[author] = 1
                else:
                    gamesPlayedMap[author] += 1
            elif wordleId == iterated_id and author in weeklyScoreMap.keys():
                weeklyScoreMap[author] += score
                if author not in gamesPlayedMap.keys():  # ugly, need to clean this up
                    gamesPlayedMap[author] = 1
                else:
                    gamesPlayedMap[author] += 1

    for user in gamesPlayedMap.keys():
        # Add seven to the score for all days missed this week
        weeklyScoreMap[user] += (days_back - gamesPlayedMap[user]) * 7
        if not userPlayedToday(user, day_of_year, filename):
            weeklyScoreMap[user] -= 7

    return sortMap(weeklyScoreMap)


def getAlltimeWinners(filename):
    # sundaysDayOfYearStart = 72 # March 13th 2022 # first sunday played...
    sundaysDayOfYearStart = 590 # August 13th 2023 - changes in Discord
    the_winners = []

    for i in range(sundaysDayOfYearStart, getDayOfYear(), 7):
        winner_map = getWordleScoreForTheWeek(i, filename)
        bestScore, winners = getWinners(winner_map)
        the_winners.append((getPrettyDay(i), bestScore, winners))
         
    return the_winners


def getWinners(winner_map):
    if len(winner_map.values()) < 1:
        return 69, ["Noone"]

    bestScore = list(winner_map.values())[0]
    winners = []
    for player in winner_map:
        if winner_map[player] is bestScore:
            winners.append(player)
    return bestScore, winners


def getLosers(winner_map):
    worstScore = list(winner_map.values())[-1]
    losers = []
    for player in winner_map:
        if winner_map[player] is worstScore:
            losers.append(player)
    return worstScore, losers 


def getDaysBack(day_of_year):
    return ((day_of_year - 3) % 7) + 1


def getTodaysWordleId(day_of_year):
    return (day_of_year + 195)  # dumb constant because of wordleId indexing


def getWordleScoreFromTxt(filename, wordleId, author):
    score_map = buildScoreMap(filename)
    score_key = buildScoreKey(wordleId, author)

    if score_key in score_map:
        return int(score_map[buildScoreKey(wordleId, author)])
    else:
        return None


def buildScoreArray(filename):
    score_arr = []
    if not os.path.exists(filename):
        return score_arr
    score_file = open(filename)
    for line in score_file:
        wordleId, author, score = line.split()
        score_arr.append((int(wordleId), author, int(score)))
    score_file.close()
    return score_arr


def buildScoreMap(filename):
    score_map = {}
    if not os.path.exists(filename):
        return score_map
    score_file = open(filename)
    for line in score_file:
        wordleId, author, score = line.split()
        score_map[buildScoreKey(wordleId, author)] = score
    score_file.close()
    return score_map


def buildScoreKey(wordleId, author):
    return "%s-%s" % (wordleId, author)


def sortMap(map):
    return dict(sorted(map.items(), key=lambda item: item[1]))


def getDayOfYear():
    return (getYearsPlayed() * 365)+int(datetime.now(gettz("America/Denver")).timetuple().tm_yday)


def getYearsPlayed():
    return 2 # Probably a better way to do this programmatically


def dayToYear(day_of_year):
    yearsSince2022 = int(day_of_year / 365)
    return str(yearsSince2022 + 2022)


def getAccurateDayOfYear(day_of_year):
    if day_of_year > 365:
        return str(day_of_year - 365)
    return str(day_of_year)


def getPrettyDay(day_of_year):
    print(dayToYear(day_of_year) + "-" + str(day_of_year))
    return datetime.strptime(dayToYear(day_of_year) + "-" + getAccurateDayOfYear(day_of_year), "%Y-%j").strftime("%m/%d/%Y")

from os import path

def getBusinessFishFile(prompt):
    file_path = "media/business-fish/" + prompt
    print(file_path)
    if path.exists(file_path + ".png"):
        return file_path + ".png"
    elif path.exists(file_path + ".gif"):
        return file_path + ".gif"
    else:
        return "media/business-fish/confused.jpg"
    return

from random import randrange

randomFailureResponses = [
    "You are the god of being bad.",
    "Just... stop... please",
    "Suck. Less.",
    "*sigh*",
    "Do you always roll this bad?",
    "I hate to say it, but that's a bad roll.",
    "Better luck next roll...",
    "You're like the nutsack of a mouse being eaten by a drowning lion."]
randomSuccessResponses = [
    "Good news everyone!",
    "GOOD SCOTT GREAT SHOT",
    "YOU ARE A GOD",
    "HAPPY HOLIDAYS!",
    "I'd rather be lucky than good.",
    "You've brought her to her milk.",
    "Sure as God's got sandals.",
    "You rolled about as good as Christopher Mersman looks.",
    "$$$ Get laid, get paid $$$"]

def parseMessage(message):
    numbers = []
    randomMax = 20
    params = message.split()
    dice = "1d20"

    if len(params) > 1:
        dice = params[1]

    if "." in dice:
        return 0, 0, True  # return that there is a decimal

    if "d" in dice:

        numOfDi = int(dice.split("d",1)[0])
        randomMax = int(dice.split("d",1)[1])
        for i in range(0 ,numOfDi):
            numbers.append(randrange(randomMax) + 1)

    return numbers, randomMax, False  # return there isn't a decimal

def getRollColor(number,best):
    color = 0x4BB543  # success
    if float(number) < .62 * best:  # warning
        color = 0xEED202
    if float(number) < .30 * best:  # failure
        color = 0xFF0000
    return color

def formatResponse(index, number, best, size):
    suffix = ""
    if size == 1:  # only enable fun responses on 1 die roll
        # print("index %d number %d best %d size %d" % (index, number, best, size))
        if number == 1 and best > 5:
            suffix = " - " + randomFailureResponses[randrange(0,len(randomFailureResponses))]
        elif number == best and best > 5:
            suffix = " - " + randomSuccessResponses[randrange(0,len(randomSuccessResponses))]

    if size == 1:
        return "**[ "+str(number)+"**/"+str(best)+" **]**" + suffix
    return "roll #"+str(index+1)+": **[ "+str(number)+"**/"+str(best)+" **]**" + suffix

def buildRollDescriptionAndColor(message):
    numbers, best, decimal = parseMessage(message.content)
    if decimal:
        return "Decimal rolls are for pussies.", 0xffc0cb

    rolledTotal = 0
    bestTotal = best * len(numbers)
    fullResponse = "**" + message.author.display_name + "'s roll!**\n\n"

    if len(numbers) == 1:
        rolledTotal = numbers[0]
        fullResponse += formatResponse(0, numbers[0], best, len(numbers))
    else:
        for i in range(0, len(numbers)):
            rolledTotal += numbers[i]
            fullResponse += "> " + formatResponse(i, numbers[i], best, len(numbers)) + "\n"
        fullResponse += "\n**Total: [ "+ str(rolledTotal) +"**/" + str(bestTotal) + " **]**"

    color = getRollColor(rolledTotal, bestTotal)
    return fullResponse, color

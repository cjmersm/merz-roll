import stockquotes
import yfinance as yf

def getStockName(message):
    return message.split(" ")[1]

def getHelp():
    return """
          > !stock(s) [ticker]
          
          **or**
          
          > !stonk(s) [ticker]
        """, 0x000000

def getColor(diff):
    color = 0x4BB543  # success
    if diff == 0:  # warning
        color = 0xEED202
    if diff < 0:  # failure
        color = 0xFF0000
    return color

def formatQuote(symbol, current_price, diff, diff_percent):
    # return ("""
    # **[%s](https://finance.yahoo.com/quote/%s) : \$%s**
    #
    # >>> ```%s```
    # """ % (symbol.upper(), symbol, current_price, history))
    return ("""
     *Stonk*: **[%s](https://finance.yahoo.com/quote/%s)**

     > Current Price      : **\$%.2f**
     > Prev Closing Price : **\$%.2f**
     > Change             : **\$%.2f**
     > Percent Change     : **%.2f%%**
     """ % (symbol.upper(), symbol, current_price, current_price - diff, diff, diff_percent))

def getBaseQuoteValue(ticker):
    return stockquotes.Stock(ticker)

def getQuote(message):
    stock_name = getStockName(message)

    if stock_name == "help":
        return getHelp()

    msft = yf.Ticker(stock_name)
    stonk = stockquotes.Stock(stock_name)
    current_price = float(stonk.current_price)
    diff = float(stonk.increase_dollars)
    diff_percent = float(stonk.increase_percent)

    return formatQuote(stock_name, current_price, diff, diff_percent), getColor(diff)



# How to start

Make sure you have docker installed
Make sure you have the discord key the Chris Mersman should provide you

Make sure Chris has added you to the testing discord channel

And then run this:
```
git clone git@gitlab.com:cjmersm/merz-roll.git
cd merz-roll
docker build . -t merz-roll-dev
docker run -it --rm --env DISCORD_KEY=<get_key_from_merzman> merz-roll-dev
```
 
# After making changes...

You can just rebuild with:
```
docker build . -t merz-roll-dev 
```
And then rerun with 
```
docker run -it --rm --env DISCORD_KEY=<get_key_from_merzman> merz-roll-dev
```

# Testing 

Currently all tests are run on docker builds. You can find them in the `tests` directory. They are
very basic standard python. Need to look into using a framework.
```
docker build . -t merz-roll-dev 
```

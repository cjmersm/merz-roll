DISCORD_USER_KEY=$1
DISCORD_CRYPTO_WEBHOOK=$2
docker stop merz-roll || true
docker rm merz-roll || true
docker image rm merz-roll || true
docker container rm merz-roll || true
docker build -t merz-roll .
docker run -d -v /root/merz-roll:/code --env DISCORD_KEY="$DISCORD_USER_KEY" --env DISCORD_CRYPTO_WEBHOOK="$DISCORD_CRYPTO_WEBHOOK" --name merz-roll merz-roll
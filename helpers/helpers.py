import re
from random import randrange

validCommands = [
    "roll", "r",
    "gif",
    "magic",
    "help",
    "test",
    "bf",
    "wlb"
]

randomErrorResponses = [
    "Shitz Fukked",
    "I Borked",
    "Errz",
    "Christopher Mersman sucks at programming...",
    "So sorry I can't process your request because I is dumbz",
    "Explosions happened and whatever you wanted didn't work",
    "My cheeks have been slapped",
    "I've failed you"
]


def getHelpMessage():
    helpMessage = "Valid merzbot commands include:"
    for command in validCommands:
        helpMessage += "\n> **!%s**" % command
    return helpMessage


def getErrorResponse():
    return randomErrorResponses[randrange(len(randomErrorResponses))]


def isWordleInput(message):
    validPattern = r"Wordle [0-9]{3} [1-6,X]/[1-6].*"
    if re.match(validPattern, message):
        return True
    return False